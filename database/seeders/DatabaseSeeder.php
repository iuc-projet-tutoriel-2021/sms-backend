<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Option;
use App\Models\Program;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Teacher;
use App\Models\Classroom;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = new Role(['name' => 'admin']);
        $role_admin->save();
        (new Role(['name' => 'cashier']))->save();
        (new Role(['name' => 'director']))->save();

        (new User([
            "uuid" => Str::uuid(),
            "first_name" => "Admin",
            "name" => "Admin",
            "born_date" => "2000/09/09",
            "born_place" => "Douala",
            "address" => "Akwa",
            "tel" => "+237 6 90 90 90 90",
            "role_id" => $role_admin->id,
            'email' => 'admin@admin.com',
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ]))->save();
        Option::factory(10)->create();
        Classroom::factory(10)->create();
        Subject::factory(10)->create();
        Teacher::factory(10)->create();
        Student::factory(10)->create();
        Program::factory(10)->create();
        User::factory(10)->create();
    }
}
