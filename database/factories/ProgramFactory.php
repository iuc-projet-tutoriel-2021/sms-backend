<?php

namespace Database\Factories;

use App\Models\Program;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProgramFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Program::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $bd = $this->faker->unixTime();
        $ed = $bd + (60 * 60 * 4);
        return [
            "begin_date" => date("Y-m-d H:i:s", $bd),
            "end_date" => date("Y-m-d H:i:s", $ed),
            "subject_id" => random_int(1, 10),
            "classroom_id" => random_int(1, 10),
            "teacher_id" => random_int(1, 20),
        ];
    }
}
