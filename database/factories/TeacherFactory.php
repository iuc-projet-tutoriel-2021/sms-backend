<?php

namespace Database\Factories;

use App\Models\Teacher;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class TeacherFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Teacher::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            "uuid" => Str::uuid(),
            "first_name" => $this->faker->firstName,
            "name" => $this->faker->name,
            "born_date" => $this->faker->date(),
            "born_place" => $this->faker->city,
            "address" => $this->faker->address,
            "tel" => $this->faker->e164PhoneNumber,
        ];
    }
}
