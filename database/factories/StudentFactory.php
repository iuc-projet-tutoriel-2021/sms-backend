<?php

namespace Database\Factories;

use App\Models\Student;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            "uuid" => Str::uuid(),
            "first_name" => $this->faker->firstName,
            "name" => $this->faker->name,
            "country" => $this->faker->country,
            "city" => $this->faker->city,
            "born_date" => $this->faker->date(),
            "born_place" => $this->faker->city,
            "address" => $this->faker->address,
            "tel" => $this->faker->e164PhoneNumber,
            "status" => ['schooling','banned'][random_int(0,1)],
            "classroom_id" => random_int(0,10),
        ];
    }
}
