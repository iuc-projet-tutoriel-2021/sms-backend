<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>IU_FICHE_{{ $student->first_name . "_" . $student->name }}.pdf</title>
    <style>
        .table{
            margin: 0 auto;
            border-collapse: collapse
        }
        .table tr td, .table tr th {
            padding: 15px;
        }
        .table tr:nth-child(even) {
            background: #dedede;
        }
        h1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <div>
        <h1>Fiche d'inscription</h1>

        <table class="table" border="1">
            <tr>
                <th>Matricule</th>
                <td>IUE00{{ $student->id }}</td>
            </tr>

            <tr>
                <th>Nom</th>
                <td>{{ $student->name }}</td>
            </tr>

            <tr>
                <th>Prénom</th>
                <td>{{ $student->first_name }}</td>
            </tr>

            <tr>
                <th>Date de naissance</th>
                <td>{{ $student->born_date }}</td>
            </tr>

            <tr>
                <th>Classe</th>
                <td>{{ $student->classroom->option->short_name . " " . $student->classroom->level }}</td>
            </tr>

            <tr>
                <th>Pays</th>
                <td>{{ $student->country }}</td>
            </tr>

            <tr>
                <th>Ville</th>
                <td>{{ $student->city }}</td>
            </tr>

            <tr>
                <th>Adresse</th>
                <td>{{ $student->address }}</td>
            </tr>

            <tr>
                <th>Numéro de téléphone</th>
                <td>{{ $student->tel }}</td>
            </tr>

            <tr>
                <th>Adresse email</th>
                <td>{{ $student->email ?? "/" }}</td>
            </tr>

        </table>
    </div>

    <script>
        (() => {
            window.print()
        })()
    </script>
</body>
</html>

