<?php


namespace App\Http\Requests;


class ClassroomRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            "level" => "required|number",
            "amount" => "required|number",
            "option_id" => "required|number",
        ];
    }
}
