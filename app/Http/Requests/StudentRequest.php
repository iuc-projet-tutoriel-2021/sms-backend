<?php

namespace App\Http\Requests;

class StudentRequest extends BaseRequest
{
    public function rules()
    {
        return [
            'first_name' => $this->sometimes . 'required|string',
            'name' => $this->sometimes . 'required|string',
            'country' => $this->sometimes . 'required|string',
            'city' => $this->sometimes . 'required|string',
            'address' => $this->sometimes . 'required|string',
            'born_date' => $this->sometimes . 'required|date_format:Y/m/d',
            'born_place' => $this->sometimes . 'required|string',
        ];
    }
}
