<?php

namespace App\Http\Controllers;

use App\Http\Requests\StudentRequest;
use App\Models\Student;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index(Request $request)
    {
        return Student::with('classroom')->get();
    }

    /**
     * @throws Exception
     */
    public function store(StudentRequest $request)
    {
        $student = new Student(array_merge($request->all(), ["uuid" => Str::uuid()]));
        if ($student->save()) {
            $student->fresh()->load('classroom');
            return response()->json($student);
        }

        throw new Exception('Unexpected Error while creating');
    }

    public function show(Student $student)
    {
        return response()->json($student);
    }

    /**
     * @throws Exception
     */
    public function update(StudentRequest $request, Student $student)
    {
        if($student->update($request->all())) {
            $student->fresh()->load('classroom');
            return response()->json($student);
        }

        throw new Exception('Unexpected Error while updating');
    }

    /**
     * @throws Exception
     */
    public function destroy(Student $student)
    {
        if ($student->delete()) {
            return [
                "success" => true,
                "deleted" => $student
            ];
        }

        throw new Exception('Unexpected Error while deleting');
    }
}
