<?php /** @noinspection ALL */

namespace App\Http\Controllers;

use App\Models\Classroom;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ClassroomController extends Controller
{
    public function index(Request $request): Collection
    {
        $level = $request->query('level');
        $result = Classroom::query()->with(['option']);
        if ($level) {
            $result = $result->where('level', $level);
        }

        return $result->get();
    }

    public function store(Request $request): bool
    {
        $classroom = new Classroom($request->all());
        if ($classroom->save()) {
            $classroom = $classroom->fresh()->load('option');
            return response()->json($classroom);
        }
    }

    public function show(Classroom $classroom)
    {
        $classroom->load('option');
        return response()->json($classroom);
    }

    public function update(Request $request, Classroom $classroom): Classroom
    {
        if ($classroom->update($request->all())) {
            $classroom->fresh()->load('option');
            return response()->json($classroom);
        }

        throw new Exception('Unexpected Error while updating');
    }

    public function destroy(Classroom $classroom): Response
    {
        $classroom = $classroom->load('option');
        if ($classroom->delete()) {
            return [
                "success" => true,
                "deleted" => $classroom
            ];
        }

        throw new Exception('Unexpected Error while deleting');
    }
}
