<?php

namespace App\Http\Controllers;

use App\Models\Student;

class NavigationController extends Controller
{
    public function print_student(Student $student) {
        $student->load(['classroom.option']);
        return view('print.student', compact('student'));
    }
}
