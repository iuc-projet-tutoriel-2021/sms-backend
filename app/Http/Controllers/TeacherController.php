<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeacherRequest;
use App\Models\Teacher;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class TeacherController extends Controller
{
    public function index(Request $request)
    {
        return Teacher::with('classrooms')->get();
    }

    /**
     * @throws Exception
     */
    public function store(TeacherRequest $request)
    {
        $teacher = new Teacher(array_merge($request->all(), ["uuid" => Str::uuid()]));
        if ($teacher->save()) {
            $teacher->fresh()->load('classrooms');
            return response()->json($teacher);
        }

        throw new Exception('Unexpected Error while creating');
    }

    public function show(Teacher $teacher)
    {
        return response()->json($teacher);
    }

    /**
     * @throws Exception
     */
    public function update(TeacherRequest $request, Teacher $teacher)
    {
        if($teacher->update($request->all())) {
            $teacher->fresh()->load('classrooms');
            return response()->json($teacher);
        }

        throw new Exception('Unexpected Error while updating');
    }

    /**
     * @throws Exception
     */
    public function destroy(Teacher $teacher)
    {
        if ($teacher->delete()) {
            return [
                "success" => true,
                "deleted" => $teacher
            ];
        }

        throw new Exception('Unexpected Error while deleting');
    }
}
