<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {
        $user = User::query()->where('email', $request->get('email'))->first();
        if (!$user || !Hash::check($request->get('password'), $user->password)) {
            return response("Authentification failed", 401);
        }

        $token = $user->createToken("User token")->plainTextToken;
        return ["user" => $user, "token" => $token];
    }

    public function register(RegisterRequest $request)
    {
        $user = new User(array_merge(
            $request->all(),
            ["password" => Hash::make($request->get('password'))]
        ));
        if ($user->save()) {
            return [
                "user" => $user,
                "token" => $user->createToken("User Token")->plainTextToken
            ];
        }
        return response("Unable to create this account.", 500);
    }
}
